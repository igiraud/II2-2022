#include "stdint.h"

typedef struct MIDI_MTrk_header MIDI_MTrk_header;
typedef struct MIDI_MThd MIDI_MThd;
typedef struct MIDI_Msg_TypeDef MIDI_Msg_TypeDef;
typedef struct Notes_TypeDef Notes_TypeDef;
typedef struct Instrument_TypeDef Instrument_TypeDef;

enum nnote                    {  DO=24, DOD,   RE,    MIB,   MI,    FA,    FAD,   SOL,   SOLD,  LA,    LAD,   SI    };

#define MIDI_NOTE_OFF        0x80
#define MIDI_NOTE_ON         0x90
#define MIDI_AFTER_KEY       0xA0
#define MIDI_CONTROL_CHANGE  0xB0
#define MIDI_PROGRAM_CHANGE  0xC0
#define MIDI_AFTER_ALL       0xD0
#define MIDI_PITCH_BEND      0xE0

#define MAX_CHANNEL 6

struct MIDI_Msg_TypeDef {
	uint8_t status;
	union {
		uint8_t data[2];
		struct {
			uint8_t key;
			uint8_t velocity;
		};
	};
};

struct MIDI_MThd {
	char type[4];
	uint32_t length;
	uint16_t format;
	uint16_t ntrks;
	uint16_t divisions;
};

struct MIDI_MTrk_header {
	char type[4];
	uint32_t length;
};

struct Notes_TypeDef {
	union {
		int16_t chanNote;
		struct {
			uint8_t channel;
			uint8_t note;
		};
	};
	uint16_t amplitude;
	uint32_t pulsation;
	uint32_t offset;
	Instrument_TypeDef* instrument;
};

struct Instrument_TypeDef {
	uint32_t periode;
	uint32_t length;
	uint32_t offset;
	uint16_t* waveform;
};
